from qgis.core import *
from PyQt4.QtGui import (QColor, QFont, QImage, QPainter)
from PyQt4.QtCore import (QSize, QSizeF)

regionLayer = QgsVectorLayer("REGIONS.js", "regions_layer", "ogr")
QgsMapLayerRegistry.instance().addMapLayer(regionLayer)

regionLayer.rendererV2().symbol().symbolLayer(0).setFillColor(QColor(255, 255, 255, 255))
regionLayer.triggerRepaint()

def setPie(layer):
        # Set histogram and diagram settings:
        pie = QgsPieDiagram()
        ds = QgsDiagramSettings()
        dColors = {'code': QColor("cyan"),
                   'autre_code': QColor("red")}
        ds.categoryColors = dColors.values()
        ds.categoryAttributes = dColors.keys()
        ds.font = QFont("Helvetica", 12)
        ds.transparency = 0
        ds.size = QSizeF(50.0, 50.0)
        ds.sizeType = 0
        ds.labelPlacementMethod = 1
        ds.scaleByArea = True
        ds.minimumSize = 0
        ds.BackgroundColor = QColor(255, 255, 255, 255)
        ds.PenColor = QColor("black")
        ds.penWidth = 0
        
        # Set renderer:
        renderer = QgsLinearlyInterpolatedDiagramRenderer()
        renderer.setUpperValue(100)  # Here you should set the maximum value of both attributes
        renderer.setUpperSize(QSizeF(20, 20))
        renderer.setLowerValue(0)
        renderer.setLowerSize(QSizeF(0, 0))
        renderer.setDiagram(pie)
        renderer.setDiagramSettings(ds)
        renderer.setClassificationAttribute(0)
        
        # Set diagram layer settings:
        layer.setDiagramRenderer(renderer)
        dls = QgsDiagramLayerSettings()
        dls.dist = 0
        dls.priority = 0
        dls.xPosColumn = -1
        dls.yPosColumn = -1
        dls.placement = 0
        
        layer.setDiagramLayerSettings(dls)
        layer.triggerRepaint()

setPie(regionLayer)

# create image
img = QImage(QSize(800, 600), QImage.Format_ARGB32_Premultiplied)

# set image's background color
color = QColor(255, 255, 255)
img.fill(color.rgb())

# create painter
p = QPainter()
p.begin(img)
p.setRenderHint(QPainter.Antialiasing)

render = QgsMapRenderer()

# set layer set
# lst = [regionLayer.id()]  # add ID of every layer
print QgsMapLayerRegistry.instance().mapLayers().keys()
render.setLayerSet(QgsMapLayerRegistry.instance().mapLayers().keys())

# set extent
rect = QgsRectangle(render.fullExtent())
rect.scale(1.1)
render.setExtent(rect)

# set output size
render.setOutputSize(img.size(), img.logicalDpiX())

# do the rendering
render.render(p)

p.end()

# save image
img.save("/tmp/render.png","png")
